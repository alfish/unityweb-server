#!/usr/bin/env node
const
  util = require('util'),
  fs = require('fs'),
  path = require('path'),
  read = require('read'),
  unitywebServer = require('./index.js');
const
  fs_ = {
    readFile: util.promisify(fs.readFile),
  },
  read_ = util.promisify(read);

(async argv => {
  const arg2 = argv[2];
  if (arg2 == '--help') {
    console.log(unitywebServer.Server.help());
    throw process.exit(0);
  }
  const fileConf = arg2 ?
    JSON.parse(await fs_.readFile(arg2, 'utf8')) :
    null;
  const conf = new unitywebServer.Configuration(fileConf);
  const dir = arg2 ? path.dirname(path.resolve(arg2)) : process.cwd();
  const server = new unitywebServer.Server(dir, conf);
  const { keyIsEncrypted } = await server.setup();
  if (keyIsEncrypted) {
    server.httpsOptions.passphrase = await read_({
      prompt: `PASSWORD "${/**@type {*}*/(conf.https).key}": `,
      silent: true,
    });
  }
  await server.start();
})(process.argv).catch(e => {
  console.error(`\x1B[31mABORT!\x1B[0m`);
  console.error(e);
  throw process.exit(1);
});
