const
  util = require('util'),
  http = require('http'),
  https = require('https'),
  connect = require('connect'),
  serveStatic = require('serve-static'),
  serveIndex = require('serve-index'),
  fs = require('fs'),
  path = require('path'),
  unitywebEncoding = require('unityweb-encoding');
const
  fs_ = {
    readFile: util.promisify(fs.readFile),
  };

function override(
  /**@type {*}*/ target,
  /**@type {*}*/ source,
) {
  for (const k in source) {
    const value = source[k];
    if (value && typeof value == 'object') {
      const obj = target[k];
      if (obj && typeof obj == 'object') {
        override(obj, value);
        continue;
      }
    }
    target[k] = value;
  }
}

class Configuration {
  /**
   * An object for HTTPS configuration.
   * @param {string} [pemPath] Path for PEM files (.key and .cer extensions are implied).
   * If not provided, HTTPS is disabled.
   * @param {number} [port] HTTPS port setting (default 443).
   */
  static HTTPS(pemPath, port) {
    return pemPath ? {
      /** HTTPS port setting. */
      port: port || 443,
      /** Path for a private key in PEM format. If encrypted, it will need a passphrase. */
      key: `${pemPath}.key`,
      /** Path for a certificate chain in PEM format. */
      cert: `${pemPath}.cer`,
    } : null;
  }

  /** Optional list of host names to log when the server starts. */
  hostNames = ['localhost'];
  /** HTTP configuration. */
  http = {
    /** HTTP port setting. */
    port: 80,
  };
  /** HTTPS configuration. */
  https = Configuration.HTTPS();
  /** A root directory from where files are served. */
  rootPath = '.';
  /** Serves files from within a given root directory, using these options. */
  serveStatic = (/**@type {serveStatic.ServeStaticOptions}*/({ index: false }));
  /** If set, serves directory listings pages for a given path, using these options. */
  serveIndex = (/**@type {serveIndex.Options?}*/({ icons: true, view: 'details' }));
  /** HTML to serve when an internal server error happens. */
  htmlInternalError = 'Internal Server Error.';
  /**
   * Create a new configuration object.
   * @param {Configuration?} [conf] The configuration parameters to override.
   */
  constructor(conf) {
    override(this, conf);
  }
}

class Server {

  static help() {
    return `\
USAGE: unityweb-server [SERVER_JSON]

With no SERVER_JSON, serve the current working directory through HTTP.
Otherwise, the JSON overrides the following default settings:
${JSON.stringify(new Configuration(), null, 2)}

To enable HTTPS, you can set the https field like this:
${JSON.stringify(Configuration.HTTPS('./server'), null, 2).replace(/\n */g, ' ')}
Port is optional. If the private key is encrypted, a password will be requested.

Check the README for more details.`;
  }

  /**
   * Create a web server for Unity WebGL projects.
   * @param {string} dir The working directory for the configuration paths.
   * @param {Configuration} conf The server configuration parameters.
   */
  constructor(dir, conf) {
    /** The working directory for the configuration paths. */
    this.dir = dir;
    /** The server configuration parameters. */
    this.conf = conf;

    /** The HTTP port (default 80). */
    this.httpPort = +conf.http.port || 80;
    /** The HTTP server created. */
    this.httpServer = null;

    /** The HTTPS port (default 443). */
    this.httpsPort = +(conf.https && conf.https.port || 443);
    /** Options for the HTTPS server. */
    this.httpsOptions = /**@type {https.ServerOptions}*/({});
    /** The HTTPS server created. */
    this.httpsServer = null;
  }

  /**
   * Sets up the server after fetching the appropriate resources.
   */
  async setup() {
    const setupResult = {};
    const pathRoot = path.resolve(this.dir, this.conf.rootPath);
    /**@type {*}*/
    const serverStatic = serveStatic(pathRoot, this.conf.serveStatic);
    /**@type {*}*/
    const serverIndex = this.conf.serveIndex && serveIndex(pathRoot, this.conf.serveIndex);
    let app = connect()
      .use(this.logRequests.bind(this))
      .use(this.httpsRedirect.bind(this))
      .use(unitywebEncoding.serveHeader(pathRoot))
      .use(serverStatic);
    if (serverIndex)
      app = app.use(serverIndex);
    /** The logic that implements this server application - a compound middleware. */
    this.app = app.use(this.handleErrors.bind(this));

    if (this.conf.https) {
      const pathKey = path.resolve(this.dir, this.conf.https.key);
      const pathCert = path.resolve(this.dir, this.conf.https.cert);
      const files_ = [
        fs_.readFile(pathKey, 'ascii'),
        fs_.readFile(pathCert, 'ascii'),
      ];
      [
        this.httpsOptions.key,
        this.httpsOptions.cert,
      ] = await Promise.all(files_);
      /** True if a passphrase is needed. Set it on `httpsOptions`. */
      setupResult.keyIsEncrypted = this.httpsOptions.key.startsWith('-----BEGIN ENCRYPTED PRIVATE KEY-----');
    } else {
      setupResult.keyIsEncrypted = false;
    }
    return setupResult;
  }

  /**
   * Starts listening for connections. The promise is resolved when the 'listening' event occurs.
   */
  async start() {
    const events_ = [];
    if (this.conf.https) {
      events_.push(new Promise((resolve, reject) => {
        this.httpsServer = https.createServer(this.httpsOptions, this.app).listen(this.httpsPort, () => {
          const port = this.httpsPort == 443 ? '' : ':' + this.httpsPort;
          for (const hostName of this.conf.hostNames)
            console.log(`SERVER https://${hostName}${port}`);
          resolve();
        }).on('error', reject);
      }));
    }
    events_.push(new Promise((resolve, reject) => {
      this.httpServer = http.createServer(this.app).listen(this.httpPort, () => {
        const port = this.httpPort == 80 ? '' : ':' + this.httpPort;
        for (const hostName of this.conf.hostNames)
          console.log(`SERVER http://${hostName}${port}`);
        resolve();
      }).on('error', reject);
    }));
    await Promise.all(events_);
  }

  /**
   * Stops the server from accepting new connections and keeps existing connections.
   * The promise is resolved when the 'close' event occurs.
   */
  async close() {
    let events_ = [];
    const httpServer = this.httpServer;
    if (httpServer) {
      events_.push(new Promise((resolve, reject) => {
        httpServer.close(err => {
          if (err) reject(err);
          else resolve();
        });
      }));
    }
    const httpsServer = this.httpsServer;
    if (httpsServer) {
      events_.push(new Promise((resolve, reject) => {
        httpsServer.close(err => {
          if (err) reject(err);
          else resolve();
        });
      }));
    }
    await Promise.all(events_);
  }

  httpsRedirect(
    /**@type {http.IncomingMessage}*/ req,
    /**@type {http.ServerResponse}*/ res,
    /**@type {(err?: any) => void}*/ next,
  ) {
    if (!this.conf.https) {
      // no https server available
      next();
    } else if (req.connection.localPort == this.httpsPort) {
      // request was via https, so do no special handling
      next();
    } else if (req.headers['upgrade-insecure-requests'] != '1') {
      // client doesn't prefer https
      next();
    } else {
      // request was via http, so redirect to https
      const host = req.headers.host;
      if (!host) {
        next();
        return;
      }
      const httpsHost = host.replace(/:\d+$/, this.httpsPort == 443 ? '' : ':' + this.httpsPort);
      const url = /**@type {*}*/(req).originalUrl;
      res.setHeader('Location', `https://${httpsHost}${url}`);
      res.writeHead(307);
      res.end();
    }
  }
  logRequests(
    /**@type {http.IncomingMessage}*/ req,
    /**@type {http.ServerResponse}*/ res,
    /**@type {(err?: any) => void}*/ next,
  ) {
    const protocol = this.conf.https && req.connection.localPort == this.httpsPort ? 'https' : 'http';
    const url = /**@type {*}*/(req).originalUrl;
    console.log(`${req.method} ${protocol}://${req.headers.host}${url}`);
    res.once('finish', () => {
      const s = res.statusCode;
      const color = s >= 500 ? 31 : s >= 400 ? 33 : 32;
      let len = res.getHeader('content-length');
      len = len != null ? ` (${len} B)` : '';
      let ctype = res.getHeader('content-type');
      ctype = ctype ? ` ${ctype}` : '';
      let cenc = res.getHeader('content-encoding');
      cenc = cenc ? ` ^${cenc}` : '';
      let loc = res.getHeader('location');
      loc = loc ? ` -> ${loc}` : '';
      console.log(`= \x1B[${color}m${s} ${res.statusMessage}\x1B[0m${len}${ctype}${cenc}${loc}`);
    });
    next();
  }
  handleErrors(
    /**@type {any}*/ err,
    /**@type {http.IncomingMessage}*/ req,
    /**@type {http.ServerResponse}*/ res,
    /**@type {(err?: any) => void}*/ next,
  ) {
    console.error('\x1B[31mERROR!\x1B[0m');
    console.error(err);
    for (const header in res.getHeaders())
      res.removeHeader(header);
    res.writeHead(500, 'Internal Server Error', {
      'Content-Type': 'text/html; charset=utf-8',
    });
    res.end(this.conf.htmlInternalError);
  }
}

module.exports = { Configuration, Server };
