# unityweb-server
A node web server for testing Unity WebGL projects.

## Command Line
```sh
unityweb-server [SERVER_JSON]
```

With no *SERVER_JSON*, serve the current working directory through HTTP.

Otherwise, the JSON overrides the following default settings:
```json
{
  "hostNames": [
    "localhost"
  ],
  "http": {
    "port": 80
  },
  "https": null,
  "rootPath": ".",
  "serveStatic": {
    "index": false
  },
  "serveIndex": {
    "icons": true,
    "view": "details"
  },
  "htmlInternalError": "Internal Server Error."
}
```

To enable HTTPS, you can set the https field like this:
```json
{ "port": 443, "key": "./server.key", "cert": "./server.cer" }
```
Port is optional. If the private key is encrypted, a password will be requested.

## Configuration

### hostNames
`string[]`  
Optional list of host names to log when the server starts.

### http.port
`number`  
HTTP port setting.

### https.port
`number`  
HTTPS port setting.

### https.key
`string`  
Path for a private key in PEM format. If encrypted, it will need a passphrase.

### https.cert
`string`  
Path for a certificate chain in PEM format.

### rootPath
`string`  
A root directory from where files are served.

### serveStatic
[`serveStatic.ServeStaticOptions`](https://www.npmjs.com/package/serve-static#options)  
Serves files from within a given root directory, using these options.

### serveIndex
[`serveIndex.Options?`](https://www.npmjs.com/package/serve-index#options)  
If set, serves directory listings pages for a given path, using these options.

### htmlInternalError
`string`  
HTML to serve when an internal server error happens.

## License
[BSD-3-Clause]( https://spdx.org/licenses/BSD-3-Clause )  
© 2019 Alfish. All rights reserved.
